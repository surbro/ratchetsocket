<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use React\Socket\Server;
use React\ZMQ\Context;
use React\EventLoop\Factory;
use MyApp\Chat;

    require dirname(__DIR__) . '/ratchetSocket/vendor/autoload.php';

    $loop   = Factory::create();	//Se creal el loop interface
    $pusher = new MyApp\Chat();	//Se crea la aplicacion que conecta todos los clientes (Recibe y envia mensajes)

    // Listen for the web server to make a ZeroMQ push after an ajax request
    $context = new Context($loop);	//Se crea el contexto para ZeroMQ
    $pull = $context->getSocket(ZMQ::SOCKET_PULL);	//Se traen los mensajes que vienen del socket ZMQ
    $pull->bind('tcp://192.168.1.170:5555'); // Binding to 192.168.1.170 la unica ip que se puede conectar
    $pull->on('message', array($pusher, 'changeOnDB'));	//Si hay un mensaje para traer, se llama la funcion changeOnDB definida en la aplicacion 

    $webSock = new Server($loop);	//Se crea un servidor con la interface del loop
    $webSock->listen(8081,'0.0.0.0'); // Binding a 0.0.0.0 significa que equipos remotos se pueden conectar

    $server =new IoServer(
        new HttpServer(
            new WsServer(
                $pusher
            )
        )
        ,$webSock
        ,$loop
    );	//Se define el servidor 

    $server->run();	//se corre el servidor

/*<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use MyApp\Chat;

    require dirname(__DIR__) . '/ratchetSocket/vendor/autoload.php';

    $server = IoServer::factory(
        new HttpServer(
            new WsServer(
                new Chat()
            )
        ),
        8081
    );

    $server->run();*/